package utils

import (
	"encoding/json"
	"net/http"
)

func ResponseJSON(w http.ResponseWriter, p interface{}, status int)  {
	ubah_ke_byte, err := json.Marshal(p);
	w.Header().Set("Content-Type", "aplication/json");
	if err != nil {
		http.Error(w, "Error", http.StatusBadRequest);
	}
	w.WriteHeader(status);
	w.Write([]byte(ubah_ke_byte));
}