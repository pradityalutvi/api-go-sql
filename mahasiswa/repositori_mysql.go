package mahasiswa

import (
	"context"
	"fmt"
	"log"
	"time"
	"database/sql"
	"errors"

	"apigosql/config"
	"apigosql/models"
)
const (
	table = "mahasiswa"
	layoutDateTime = "2006-01-02 15:04:05"
)

//get All
func GetAll(ctx context.Context) ([]models.Mahasiswa, error) {
	var data_mhs []models.Mahasiswa

	db, err := config.MySQL()
	if err != nil{
		log.Fatal("Cant connect to MySQL", err)
	}

	query := fmt.Sprintf("select * from %v order by id desc", table)
	result_query, err := db.QueryContext(ctx, query)

	if err != nil{
		log.Fatal(err);
	}

	for result_query.Next(){
		var mahasiswa models.Mahasiswa
		var  created_at, updated_at string

		if err = result_query.Scan(
			&mahasiswa.Id,
			&mahasiswa.Nim,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&created_at,
			&updated_at,
		); err != nil{
			return nil, err
		}
		
		mahasiswa.Created_at, err = time.Parse(layoutDateTime, created_at);
		if err != nil{
			log.Fatal(err);
		}
		mahasiswa.Updated_at, err = time.Parse(layoutDateTime, updated_at);
		if err != nil{
			log.Fatal(err);
		}
		data_mhs = append(data_mhs, mahasiswa)
	}
	return data_mhs, nil
}

//Insert Data
func Insert(ctx context.Context, mhs models.Mahasiswa) error  {
	db, err := config.MySQL();

	if err != nil{
		log.Fatal("Cant connect to MySQL", err)
	}

	query := fmt.Sprintf("insert into %v (nim, name, semester, created_at, updated_at) values(%v,'%v',%v,'%v','%v')",table, mhs.Nim, mhs.Name, mhs.Semester, time.Now().Format(layoutDateTime), time.Now().Format(layoutDateTime));
	_, err = db.ExecContext(ctx, query);

	if err != nil {
		return err
	}
	return nil
}

//Update Data
func Update(ctx context.Context, mhs models.Mahasiswa) (int64, error)  {
	db, err := config.MySQL();

	if err != nil{
		log.Fatal("Cant connect to MySQL", err)
	}

	query := fmt.Sprintf("update %v set nim = %d, name = '%s', semester = %d, updated_at = '%v' where id = %d",
	table,
	mhs.Nim,
	mhs.Name,
	mhs.Semester,
	time.Now().Format(layoutDateTime),
	mhs.Id);
	res, err := db.ExecContext(ctx, query);
	affected,_ := res.RowsAffected()
	// fmt.Println(affected)
	if err != nil {
		return 0, err
	}
	if affected == 0{
		return affected, errors.New("Tidak ada data yang diperbarui, Id tidak ditemukan")
	}
	return affected, nil
}

//Delete Data
func Delete(ctx context.Context, mhs models.Mahasiswa) (models.Mahasiswa, error)  {
	db, err := config.MySQL();

	if err != nil{
		log.Fatal("Cant connect to MySQL", err)
	}
	
	data_deleted, _ := Detail(ctx, mhs)
	// fmt.Println(data_deleted)
	
	query := fmt.Sprintf("delete from %v where id = %d",table,mhs.Id);
	// fmt.Println(query);
	result, err := db.ExecContext(ctx, query);
	if err != nil && err != sql.ErrNoRows {
		return models.Mahasiswa{}, err
	}

	check, err := result.RowsAffected()
	if check == 0{
		return models.Mahasiswa{}, errors.New("id tidak tersedia")
	}
	return data_deleted, nil
}

//Detail Mahasiswa
func Detail(ctx context.Context, mhs models.Mahasiswa) (models.Mahasiswa, error) {
	db, err := config.MySQL()
	if err != nil{
		log.Fatal("Cant connect to MySQL", err)
	}

	query := fmt.Sprintf("select * from %v where id = %d", table, mhs.Id)
	result_query, err := db.QueryContext(ctx, query)
	if err != nil{
		log.Fatal(err);
	}
	defer result_query.Close()

	var detail_mhs models.Mahasiswa
	var  created_at, updated_at string

	for result_query.Next(){
		if err = result_query.Scan(&detail_mhs.Id,&detail_mhs.Nim,&detail_mhs.Name,&detail_mhs.Semester,&created_at,&updated_at); err != nil{
			return models.Mahasiswa{}, err
		}
		detail_mhs.Created_at, err = time.Parse(layoutDateTime, created_at);
		if err != nil{
			log.Fatal(err);
		}
		detail_mhs.Updated_at, err = time.Parse(layoutDateTime, updated_at);
		if err != nil{
			log.Fatal(err);
		}
	}
	return detail_mhs, nil
}