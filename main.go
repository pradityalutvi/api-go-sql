package main
import (
	"apigosql/utils"
	"apigosql/mahasiswa"
	"apigosql/models"
	
	"context"
	"fmt"
	"log"
	"net/http"
	"encoding/json"
	"strconv"
)
const port = ":8000"
func main() {
	http.HandleFunc("/mahasiswa", GetMahasiswa)
	http.HandleFunc("/mahasiswa/create", CreateMahasiswa)
	http.HandleFunc("/mahasiswa/update", UpdateMahasiswa)
	http.HandleFunc("/mahasiswa/delete", DeleteMahasiswa)
	fmt.Printf("Listen and serve at %v", port)
	err := http.ListenAndServe(port, nil);
	if err != nil {
		log.Fatal(err);
	}
}

// Get Mahasiswa
func GetMahasiswa(w http.ResponseWriter, r *http.Request)  {
	if r.Method == "GET"{
		ctx, cancel := context.WithCancel(context.Background());
		defer cancel();
		data_mhs, err := mahasiswa.GetAll(ctx);
		if err != nil{
			fmt.Println(err)
		}
		response := models.Response{
			Status : 1,
			Message : "Success",
			Data : data_mhs,
		}
		utils.ResponseJSON(w, response,http.StatusOK)
		return
	}
	http.Error(w, "Tidak Diijinkan", http.StatusNotFound)
	return
}

// Create Mahasiswa
func CreateMahasiswa(w http.ResponseWriter, r *http.Request)  {
	if r.Method == "POST"{
		if r.Header.Get("Content-Type") != "application/json"{
			http.Error(w, "Gunakan contenttype application/json", http.StatusBadRequest)
			return
		}
		ctx, cancel := context.WithCancel(context.Background());
		defer cancel();

		var mhs models.Mahasiswa
		if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil{
			utils.ResponseJSON(w, err,http.StatusBadRequest)
			return
		}
		
		if err := mahasiswa.Insert(ctx,mhs); err != nil{
			utils.ResponseJSON(w, err,http.StatusInternalServerError)
			return
		}
		res := map[string]string{
			"status" : "Inserting Data Success",
		}
		utils.ResponseJSON(w, res,http.StatusCreated)
		return
	}
	http.Error(w, "Tidak Diijinkan", http.StatusMethodNotAllowed)
	return
}

//Update Mahasiswa
func UpdateMahasiswa(w http.ResponseWriter, r *http.Request)  {
	if r.Method == "PUT"{
		if r.Header.Get("Content-Type") != "application/json"{
			http.Error(w, "Gunakan contenttype application/json", http.StatusBadRequest)
			return
		}
		ctx, cancel := context.WithCancel(context.Background());
		defer cancel();

		var mhs models.Mahasiswa
		if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil{
			utils.ResponseJSON(w, err,http.StatusBadRequest)
			return
		}
		
		updated, err := mahasiswa.Update(ctx,mhs);
		if err != nil{
			res := map[string]string{
				"error" : fmt.Sprintf("%s", err),
				"affectedRows" : fmt.Sprintf("%d", updated),
			}
			utils.ResponseJSON(w, res,http.StatusInternalServerError)
			return
		}
		res := map[string]string{
			"message" : "Updating Data Success",
			"affectedRows" : fmt.Sprintf("%d", updated),
		}
		utils.ResponseJSON(w, res,http.StatusCreated)
		return
	}
	http.Error(w, "Tidak Diijinkan", http.StatusMethodNotAllowed)
	return
}

//Delete Mahasiswa
func DeleteMahasiswa(w http.ResponseWriter, r *http.Request)  {
	if r.Method == "DELETE"{
		ctx, cancel := context.WithCancel(context.Background());
		defer cancel();

		var mhs models.Mahasiswa
		id := r.URL.Query().Get("id")

		if id == ""{
			utils.ResponseJSON(w, "Field ID required",http.StatusBadRequest)
			return
		}
		mhs.Id, _ = strconv.Atoi(id)
		deleted, err := mahasiswa.Delete(ctx,mhs);
		if err != nil{
			response := models.Response{
				Status : 0,
				Message : fmt.Sprintf("%v", err),
				Data	: nil,
			}
			utils.ResponseJSON(w, response,http.StatusBadRequest)
			return
		}
		var data []models.Mahasiswa
		data = append(data, deleted)
		response := models.Response{
			Status : 1,
			Message : "Deleting Data Success",
			Data	: data,
		}

		utils.ResponseJSON(w, response,http.StatusCreated)
		return
	}
}