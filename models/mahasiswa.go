package models

import (
	"time"
)

type (
	Mahasiswa struct {
		Id			int `json:"id"`;
		Nim			int `json:"nim"`;
		Name		string `json:"name"`;
		Semester	int `json:"semester"`;
		Created_at	time.Time `json:"created_at"`;
		Updated_at	time.Time `json:"updated_at"`;
	}

	Response struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
		Data    []Mahasiswa
	}
)